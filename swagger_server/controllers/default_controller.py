from datetime import datetime,timedelta,date,time
import connexion
import six
import time
from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501
from swagger_server.models.inline_response2001 import InlineResponse2001  # noqa: E501
from swagger_server.models.inline_response2002 import InlineResponse2002  # noqa: E501
from swagger_server.models.inline_response2003 import InlineResponse2003  # noqa: E501
from swagger_server.models.inline_response2004 import InlineResponse2004  # noqa: E501
from swagger_server.models.inline_response2005 import InlineResponse2005  # noqa: E501
from swagger_server.models.inline_response2006 import InlineResponse2006  # noqa: E501
from swagger_server.models.inline_response400 import InlineResponse400  # noqa: E501
from swagger_server.models.inline_response4001 import InlineResponse4001  # noqa: E501
from swagger_server.models.inline_response4002 import InlineResponse4002  # noqa: E501
from swagger_server.models.inline_response404 import InlineResponse404  # noqa: E501
from swagger_server.models.inline_response200_array_closures_points import InlineResponse200ArrayClosuresPoints
from swagger_server.models.inline_response2005 import InlineResponse2001ArrayClosures
from swagger_server import util
import swagger_server.management_system2  as mgmt
import json,asyncio

Management_System = mgmt.ManagementSystem()
def clear_n_closures(number_closure):  # noqa: E501
    closures_keep = Management_System.delete_n_closures(number_closure)

    return InlineResponse2002(closures_keep)


def closures(number_of_closure):  # noqa: E501
    response = []
    last_closures = Management_System.show_closures(number_of_closure)
    for r in last_closures:
            for l in r:
                response.append(l)
    # print(response)
    return InlineResponse2005(InlineResponse2001ArrayClosures(response))

def closures_closure_id_farmers_points_startend_get(closure_id, start, end):  # noqa: E501
    closure = []
    points = []
    hashes = []
    rc = []
    rp = []
    closures= Management_System.get_farmer_points_by_closure_id(closure_id, start, end)   
    # InlineResponse2006(InlineResponse200ArrayClosuresPoints(closures[0][0],closures[0][1]))

    # print(points)
    for i in closures:
        hashes.append(i[0])
        closure.append(i[1])

        # InlineResponse2006(InlineResponse200ArrayClosuresPoints(closures[i][0],closures[i][1]))
    return InlineResponse2006(InlineResponse200ArrayClosuresPoints(hashes,closure))
def closures_create(start,end):  # noqa: E501
    hash_closure = Management_System.create_closure(start,end)
    print(f"Closure creado correctament hash : {hash_closure} y puntos de farmer setados a 0")
    if hash_closure is None or hash_closure == 0:
        print("No rewards to distribute, exiting")
        return 0
    timestamp_start = date(year=2021,month=10,day=5)
    timestamp_end = end
    workers_inserted =Management_System.closures_import(hash_closure,timestamp_start,date.today()) # hash de prueba aa4831cfda9462c5b399a9c40ed8413e
    print(f"Total workers insertados en el sistema de pagos despues de crear el cierre : {workers_inserted}")

    return InlineResponse200(workers_inserted, hash_closure)


def closures_payout(closure_hash):  # noqa: E501
    monto,monto_with_fee = Management_System.pay_closures_with_fe(closure_hash,0.1)
    print(f"El monto añadido para pagar es  : {monto}, el monto total sin descontar el fee es : {monto_with_fee}")
  
    return InlineResponse2001(f"El monto añadido para pagar es  : {monto}, el fee es: {monto_with_fee-monto}")


def parcial_delete(time_utc):  # noqa: E501
    partial_count = Management_System.delete_parials_time(time_utc)
    
    return InlineResponse2004(partial_count)


def partial_import(time_utc):  # noqa: E501
    
    response = Management_System.partial_import(time_utc)
    """partial_import

    Importa los parciales hasta {time_utc} a el histórico de parciales # noqa: E501

    :param time_utc: time_UTC
    :type time_utc: str

    :rtype: InlineResponse2003
    """
    return InlineResponse2003(response)
