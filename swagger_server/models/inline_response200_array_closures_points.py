# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server import util


class InlineResponse200ArrayClosuresPoints(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, closure_hash: str=None, accumulated_points: float=None):  # noqa: E501
        """InlineResponse200ArrayClosuresPoints - a model defined in Swagger

        :param closure_hash: The closure_hash of this InlineResponse200ArrayClosuresPoints.  # noqa: E501
        :type closure_hash: str
        :param accumulated_points: The accumulated_points of this InlineResponse200ArrayClosuresPoints.  # noqa: E501
        :type accumulated_points: float
        """
        self.swagger_types = {
            'closure_hash': str,
            'accumulated_points': float
        }

        self.attribute_map = {
            'closure_hash': 'closure-hash',
            'accumulated_points': 'accumulated-points'
        }
        self._closure_hash = closure_hash
        self._accumulated_points = accumulated_points

    @classmethod
    def from_dict(cls, dikt) -> 'InlineResponse200ArrayClosuresPoints':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The inline_response_200_ArrayClosuresPoints of this InlineResponse200ArrayClosuresPoints.  # noqa: E501
        :rtype: InlineResponse200ArrayClosuresPoints
        """
        return util.deserialize_model(dikt, cls)

    @property
    def closure_hash(self) -> str:
        """Gets the closure_hash of this InlineResponse200ArrayClosuresPoints.


        :return: The closure_hash of this InlineResponse200ArrayClosuresPoints.
        :rtype: str
        """
        return self._closure_hash

    @closure_hash.setter
    def closure_hash(self, closure_hash: str):
        """Sets the closure_hash of this InlineResponse200ArrayClosuresPoints.


        :param closure_hash: The closure_hash of this InlineResponse200ArrayClosuresPoints.
        :type closure_hash: str
        """

        self._closure_hash = closure_hash

    @property
    def accumulated_points(self) -> float:
        """Gets the accumulated_points of this InlineResponse200ArrayClosuresPoints.


        :return: The accumulated_points of this InlineResponse200ArrayClosuresPoints.
        :rtype: float
        """
        return self._accumulated_points

    @accumulated_points.setter
    def accumulated_points(self, accumulated_points: float):
        """Sets the accumulated_points of this InlineResponse200ArrayClosuresPoints.


        :param accumulated_points: The accumulated_points of this InlineResponse200ArrayClosuresPoints.
        :type accumulated_points: float
        """

        self._accumulated_points = accumulated_points
