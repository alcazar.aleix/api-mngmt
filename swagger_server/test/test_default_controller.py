# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501
from swagger_server.models.inline_response2001 import InlineResponse2001  # noqa: E501
from swagger_server.models.inline_response2002 import InlineResponse2002  # noqa: E501
from swagger_server.models.inline_response2003 import InlineResponse2003  # noqa: E501
from swagger_server.models.inline_response2004 import InlineResponse2004  # noqa: E501
from swagger_server.models.inline_response2005 import InlineResponse2005  # noqa: E501
from swagger_server.models.inline_response2006 import InlineResponse2006  # noqa: E501
from swagger_server.models.inline_response400 import InlineResponse400  # noqa: E501
from swagger_server.models.inline_response4001 import InlineResponse4001  # noqa: E501
from swagger_server.models.inline_response4002 import InlineResponse4002  # noqa: E501
from swagger_server.models.inline_response404 import InlineResponse404  # noqa: E501
from swagger_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_clear_n_closures(self):
        """Test case for clear_n_closures

        Dado un numero de cierres (number_closure) elimina todos los cierres salvo el numero de ultimos cierres que queramos gaurdar.
        """
        response = self.client.open(
            '/v1//closures/clear/{number_closure}'.format(number_closure=1.2),
            method='POST')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_closures(self):
        """Test case for closures

        
        """
        response = self.client.open(
            '/v1//closures/count/{number_of_closure}'.format(number_of_closure=1.2),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_closures_closure_id_farmers_points_startend_get(self):
        """Test case for closures_closure_id_farmers_points_startend_get

        
        """
        response = self.client.open(
            '/v1//closures/{closure-id}/farmers/points/{start},{end}'.format(closure_id=1.2, start=1.2, end=1.2),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_closures_create(self):
        """Test case for closures_create

        Crea el cierre en chia-pool-protocol, luego lo importa a la base database-pool
        """
        response = self.client.open(
            '/v1//closure/create',
            method='POST')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_closures_payout(self):
        """Test case for closures_payout

        Dado un hash de cierre ( closure-hash ) procesa los puntos de los farmers generando los pagos, para luego realizar los pagos mediante payuout-coin
        """
        response = self.client.open(
            '/v1//closures/{closure-hash}/payout'.format(closure_hash='closure_hash_example'),
            method='POST')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_parcial_delete(self):
        """Test case for parcial_delete

        
        """
        response = self.client.open(
            '/v1//parcial/delete/{time-utc}'.format(time_utc='time_utc_example'),
            method='POST')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_partial_import(self):
        """Test case for partial_import

        
        """
        response = self.client.open(
            '/v1//parcial/import/{time-utc}'.format(time_utc='time_utc_example'),
            method='POST')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
