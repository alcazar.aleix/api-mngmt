import asyncio
from asyncio import Task
from math import floor
from typing import Dict, Optional, Set, List, Tuple, Callable
import os
import yaml
from datetime import datetime
import hashlib
from pathlib import Path
import sqlite3
import asyncpg
from datetime import date
from datetime import datetime
import time
import math
from blspy import AugSchemeMPL, G1Element
import psycopg2
import pymysql
from chia.types.blockchain_format.sized_bytes import bytes32
import requests
from swagger_server.coin_record import CoinRecord

class ManagementSystem:
    def __init__(self):
        with open(os.getcwd() + "/config.yaml") as f:
            pool_config: Dict = yaml.safe_load(f)
        self.pool_config = pool_config

        self.pool_config = pool_config
        config_path_db = r"C:\Users\Aleix\Desktop\api_mgmt\api_mgmt\swagger_server\pooldb.sqlite"
        self.connection: Optional[pymysql.Connection] = None
        print(f"Configurando base de datos: {config_path_db}")

        if (config_path_db) :
            self.db_path = Path(config_path_db)
        else:
            self.db_path = Path("pooldb.sqlite")
        self.payment_interval = pool_config["payment_interval"]

        print("Preparando base de datos sqlite y paydb de prueba")
        self.connection_payments: Optional[psycopg2.Connection] = None
        self.pool_fee = pool_config["pool_fee"]   
        self.scan_p2_singleton_puzzle_hashes: Set[bytes32] = set()
        self.pending_payments: Optional[asyncio.Queue] = None
        self.number_of_partials_target: int = pool_config["number_of_partials_target"]

    def connect_payments_system(self):
        host=self.pool_config["payouts"]["database"]["host"]
        port=self.pool_config["payouts"]["database"]["port"]
        user=self.pool_config["payouts"]["database"]["user"]
        password=self.pool_config["payouts"]["database"]["password"]
        db=self.pool_config["payouts"]["database"]["db"]

        print(f" PUERTO {port}  ")

        print(f" {host}  {port}   {user}   {password}   {db}  ")
        self.connection_payments = psycopg2.connect(host=host,port=port,user=user,password=password,database=db)
        return self.connection_payments
    
    def connect_mariaDB_system(self):
        hostM=self.pool_config["mariadb"]["database"]["host"]
        portM=self.pool_config["mariadb"]["database"]["port"]
        userM=self.pool_config["mariadb"]["database"]["user"]
        passwordM=self.pool_config["mariadb"]["database"]["password"]
        dbM=self.pool_config["mariadb"]["database"]["db"]

        print(f" PUERTO {portM}  ")

        print(f" {hostM}  {portM}   {userM}   {passwordM}   {dbM}  ")
        self.connection = pymysql.connect(host=hostM,port=portM,user=userM,password=passwordM,database=dbM)
        return self.connection
    def create_closure(self,start,end):
        hash=""
        try:

            self.connection = self.connect_mariaDB_system()
            segundos, timestamp = math.modf(datetime.timestamp(datetime.utcnow()))
            hash = hashlib.md5()
            hash.update(str(timestamp).encode())
            hash = hash.hexdigest()

            cursor =  self.connection.cursor()
            print("Sumarizamos los rewards y guardamo el monto completo")
            cursor.execute("SELECT SUM(amount) from reward")
            for t in cursor.fetchone():
                total_amount_claimed = t
                print(t)
            print(total_amount_claimed)
            if total_amount_claimed == None: 
                print("No rewards to distribute")
                return 0
            print(f"Agrego closures")
            cursor.execute(f"INSERT INTO closures (hash, `timestamp`, accumulated_amount) VALUES('{hash}', CURRENT_TIMESTAMP, {total_amount_claimed})")
            print(f"Agrego los puntos de los farmers")
            cursor.execute(f"SELECT MAX(hash) from closures")
            print(cursor.fetchone())
            cursor.execute(
                f"REPLACE INTO farmer_points_of_closure (launcher_id, points, closure_hash) SELECT launcher_id, points ,'{hash}' "\
                f"FROM farmer WHERE is_pool_member = 1"
            )#updte on duplicate key
            
            print(f"No actualizamos puntos ya que es de prueba")
            # cursor.execute( f"UPDATE farmer SET points=0") <-- CAMBIAR!!
            self.connection.commit()
            print("Record inserted successfully into MARIADB_developers table ", cursor.rowcount)
            cursor.close()

        except Exception as error:
            print("Failed to insert data into MARIA DB table", error)
        finally:
            if self.connection:
                self.connection.close()
                print("The Maria DB connection is closed")
        return hash
    
    def farmer_update_state(self,launcher_id,new_state,description):#farmer and points update
        try: #añadir el volcado de puntos de cierre, hash del cierre, informacion del cierre, etc en la base de pagos en esta funcion 
            self.connection_payments = self.connect_payments_system()
            
            self.connection_payments.execute('''INSERT INTO public.states_of_workers
                                                            (id, description, active)
                                                        values ( (SELECT current_state_of_worker FROM public.workers where worker_coind_id = '($1)'),
                                                            '($2)',
                                                            '($3)')                                                             
                                                    ''',launcher_id,description, new_state)

            self.connection_payments.execute('''INSERT INTO public.historical_states_of_workers
                                                            (datetime, states_of_worker,worker)
                                                        values ( CURRENT_TIMESTAMP,
                                                            (SELECT id FROM public.states_of_workers st where st.description = '($2)'),
                                                            (SELECT id FROM public.workers where worker_coind_id = '($1)'))
                                                    ''',launcher_id,description)#id autoincremental
        except asyncpg.Error as error:
            print("Failed to insert data into worker_update_state", error)
        finally:
            if self.connection_payments:
                self.connection_payments.close()
                print("The PostgreSQL connection is closed")

    def farmer_import(self,launcher_id):
        rows = []
        rows_p = []
        timestamps = []
        # recupero el farmers en la base local para validar que exista
        #importamos los parciales des del ultimo cierre
        try:
            self.connection = self.connect_mariaDB_system()
            # self.connection = sqlite3.connect(self.db_path)
            cursor = self.connection.cursor()
            cursor.execute( f"select * from farmer f where f.launcher_id =  '{launcher_id}'")
            rows= cursor.fetchall()
            cursor.execute(f"select COUNT(*) from partial p where p.launcher_id = '{launcher_id}' and `timestamp` > (UNIX_TIMESTAMP()-86400)") # and  > penultimo timestamp
            for c in cursor.fetchone():
                number_of_partials_worker = c
            print(number_of_partials_worker)
            # cursor.execute(f"select timestamp from partial p where p.launcher_id = '{launcher_id}'")
            # for i,t in cursor.fetchall():
            #     timestamps[i] = t
            # rows_partials = cursor.fetchall()
            # timestamp_partial_worker = rows_partials[1]
            # difficulty_partial_worker = rows_partials[2]
            # print(timestamp_partial_worker)
            # print(difficulty_partial_worker)
        except Exception as error:
            print("Failed to select data into mariaDB table", error)
        finally:
            if self.connection:
                self.connection.close()
                print("The  MARIADB connection is closed")
        if number_of_partials_worker == 0: 
            print(f"{launcher_id} no tiene parciales recientes, parece estar inactivo ")
            
        if len(rows) == 0: 
            print(f"{launcher_id} no existe en la base de datos de farmers ")
            return 0

        if not rows[0][10]: 
            print(f"{launcher_id} no esta activo en esta pool")
            return 0

        farmer_chia = rows[0]

        wallet_hash = farmer_chia[9]
        print(f"{wallet_hash} del farmer -> payout instructions")

        # importo el farmer y todos los datos
        print(f"Ingresando el farmer {launcher_id} al sistema de pagos")

        worker_id_insert = 0
        print(f"busamos si existe el worker en el sistema de pagos")
        
        try:
            self.connection_payments = self.connect_payments_system()
            cursor2 = self.connection_payments.cursor()
            
            cursor2.execute(f"SELECT pm.interval_of_metric from profiles_metrics pm inner join coins c on (pm.coin = c.id)"\
                f" inner join workers w on (c.id = w.coin)"\
                f" inner join metrics m on (m.worker = w.id)")
            for i in cursor2.fetchone():
                interval = i
            cursor2.execute(f"SELECT COUNT(*) from reward r inner join workers w on r.worker = w.id where r.timestamp > NOW() - INTERVAL '{interval} HOURS' and w.worker_coin_id = '{launcher_id}'") # podem consultar per id no launcher
            for b in cursor2.fetchone():
                number_of_blocks_worker = b
            print(number_of_blocks_worker)
            print("Insertando metricas basadas en la informacion del worker")
            query =  f"INSERT INTO public.metrics (metric_1, metric_2, metric_3, worker)"\
                f" VALUES (({float(number_of_partials_worker)}),"\
                f" ({float(number_of_blocks_worker)}),"\
                f" (123.0),"\
                f" (select workers.id from workers where workers.worker_coin_id='{launcher_id}' ))"
            cursor2.execute( query )
            self.connection_payments.commit()
            query = f"SELECT w.worker_coin_id from public.workers w where w.worker_coin_id='{launcher_id}'" 
            cursor2.execute(query )
            # for i in cursor2.fetchone():
            #     inserteds = i 
            # print(inserteds)
            
            if len(cursor2.fetchall()):
                print(f"Worker ya insertado, no vamos a insertar ni worker, ni wallet ni wallet of worker, worker id insert es 0")
                return 
                
            else: 
                print(f"Insertando worker nuevo ")
                query =  f"INSERT INTO public.workers (coin, worker_coin_id, state, datetime_state)"\
                    f" VALUES((select id from coins where description = 'CHIA'),"\
                    f"('{launcher_id}'::bpchar),"\
                    f"(select id from states_of_workers where description = 'activo'),"\
                    f"CURRENT_TIMESTAMP)"
                print(query)
                cursor2.execute( query )
                self.connection_payments.commit()

            print(f"Recupero worker id")
            worker_id_insert = cursor2.execute(f"SELECT id FROM workers where workers.worker_coin_id = '{launcher_id}'")
            for wii in cursor2.fetchone():
                worker_id_insert = wii
            print(worker_id_insert)
            
            print(f"Insertando estado con fecha en historicos - datetime_state- ")
            query = f"INSERT INTO public.historical_states_of_workers (datetime, state, worker)"\
                f" VALUES(CURRENT_TIMESTAMP,"\
                f"(select states_of_workers.id from states_of_workers inner join workers on states_of_workers.id = workers.state where workers.id = {worker_id_insert}),"\
                f"(select workers.id from workers where worker_coin_id = '{launcher_id}'::bpchar))"# Esta consulta es solo para comprobar que el id del workers que nos saca es el mismo que el worker_id_insert que usamos arriba, despues cambia
            print(query)
            cursor2.execute( query )
            self.connection_payments.commit()
            print("Recupero id de estado de farmer")
            query = f"SELECT MAX(id) FROM historical_states_of_workers"
            cursor2.execute( query )
            for hi in cursor2.fetchone():
                worker_state_id = hi
            print(worker_state_id)
            
            print(f"Insertando wallet")
            query =  f"INSERT INTO public.wallets (description, datetime_create, hash, coin)"\
                f" VALUES('chia_wallet', CURRENT_TIMESTAMP, '{wallet_hash}'::bpchar, (select id from coins where description = 'CHIA'))"
            print(query)
            cursor2.execute( query )
            self.connection_payments.commit()
            print(f"Recupero ID de wallet")
            wallet_id_insert = cursor2.execute("SELECT MAX(id) FROM wallets")
            for widi in cursor2.fetchone():
                wallet_id_insert = widi
            print(wallet_id_insert)

            print("Insertando wallet of worker")
            query = "INSERT INTO public.wallets_of_workers (worker, wallet, description, datetime_create, state, datetime_state)"\
                f" VALUES({worker_id_insert},"\
                f" {wallet_id_insert},"\
                f"'chia wallet'::bpchar,"\
                f" CURRENT_DATE,"\
                f" (select states_of_wallets_of_workers.id from states_of_wallets_of_workers where description = 'activa'),"\
                f" CURRENT_TIMESTAMP)"
            cursor2.execute(query)
            self.connection_payments.commit()
            print(f"insertando estado de wallet of workers en historicos")
            query =  f"INSERT INTO public.historical_states_of_wallets_of_workers (datetime, state, worker, wallet)"\
                f" VALUES(CURRENT_TIMESTAMP,"\
                f"(select states_of_wallets_of_workers.id from states_of_wallets_of_workers where description = 'activa'),"\
                f"{worker_id_insert},"\
                f"{wallet_id_insert})"
            cursor2.execute(query)
            self.connection_payments.commit()

            print(f"Recupero ID de estado de wallet")
            state_wallet = cursor2.execute("SELECT MAX(id) FROM historical_states_of_wallets_of_workers")
            # state_wallet = cursor2.fetchone()
            for sw in cursor2.fetchone():
                state_wallet = sw
            print(state_wallet)
            self.connection_payments.commit()
            print("Transaction completed successfully ")
            
        except Exception as e:
            print(f"Error inserting {e}")
            # self.connection_payments.rollback()
            worker_id_insert = 0
        
        # finally:
        #     # closing database connection.
        #     if self.connection_payments:
        #         cursor2.close()
        #         self.connection_payments.close()
        return worker_id_insert

    def closures_import(self,closure_hash,time_start,time_end):
        # self.connection = sqlite3.connect(self.db_path)
        phs_to_pay = []
        self.connection = self.connect_mariaDB_system() 
        self.connection_payments = self.connect_payments_system()
        cursor2 = self.connection_payments.cursor()
        cursor = self.connection.cursor()
        farmers_count = 0

        # importar cierre
        ## recueperar cierre
        print("recuperamos el cierre!")
        query = f" SELECT hash, 'timestamp', accumulated_amount FROM closures where hash = '{closure_hash}'"
        print(f" recuperando cierre: {query}")
        cursor.execute(query)
        rows= cursor.fetchall()
        if len(rows) == 0: 
            print(f"{closure_hash} no existe en la base de datos ")
            exit

        closure_timestamp = rows[0][1]
        # closure_datetime = datetime.fromtimestamp(closure_timestamp)

        closure_amount = rows[0][2]

        print(f" la marca de tiempo es {closure_timestamp} ")
        print(f" el monto es {closure_amount} ")

        ## guardar cierre en el sistema de pagos
        try:
            
            print(f"Importando cierre")
            query = f" INSERT INTO public.closures (datetime_state, state, hash, datetime_start, datetime_end) "\
                f" VALUES(CURRENT_TIMESTAMP,"\
                f" (select id from states_of_closures where description = 'created'),"\
                f"('{closure_hash}'::bpchar),"\
                f"(TIMESTAMP '2021-10-03 15:36:38'),"\
                f"(TIMESTAMP '2021-10-07 15:36:38'))" 
            print(query)
            cursor2.execute( query )
            
            print(f"Recupero cierre")
            query = "SELECT MAX(id) FROM closures"
            cursor2.execute( query )
            for n in cursor2.fetchone():
                closures_id_ps = n
            print(f"Cierre importado id: {closures_id_ps}")
            segundos, timestamp = math.modf(datetime.timestamp(datetime.utcnow()))
            closure_hash_mining_instance = hashlib.md5()
            closure_hash_mining_instance.update(str(timestamp).encode())
            closure_hash_mining_instance = closure_hash_mining_instance.hexdigest()
            
            print("Insertando cierre en el cierres de instancia de minado (closures_mining_instances)")
            query = "INSERT INTO closures_of_mining_instance (hash, datetime, closure, mining_instance, state, datetime_state)"\
                f" VALUES('{closure_hash_mining_instance}',CURRENT_TIMESTAMP,"\
                f" (select closures.id from closures where hash = '{closure_hash}'),"\
                f" (select mining_instances.id from mining_instances inner join coins on mining_instances.coin=coins.id where coins.description = 'CHIA'),"\
                f" (select states_of_closures_mi.id from states_of_closures_mi where description = 'imported'),"\
                f" CURRENT_TIMESTAMP)"
            print(query)
            cursor2.execute( query )
            self.connection_payments.commit()
            print(f"Recupero id de cierre de instancia de minado")
            query = "SELECT MAX(id) FROM closures_of_mining_instance"
            cursor2.execute( query )
            for cip in cursor2.fetchone():
                closures_id_mi = cip
            print("insertando historical states of closure MINING INSTANCE")
            query = "INSERT INTO historical_states_of_mining_instance (datetime, state, mining_instance)"\
                f" VALUES(CURRENT_TIMESTAMP,"\
                f" (select somi.id from states_of_mining_instance somi where somi.description = 'activo' ),"\
                f" (select comi.mining_instance from closures_of_mining_instance comi inner join closures cr on comi.closure = cr.id where cr.hash = '{closure_hash}'))"
            cursor2.execute( query )
            
            print("insertando historical states of closure relacionado con state")
            query = "INSERT INTO historical_states_of_closures (datetime, state, closure)"\
                f" VALUES(CURRENT_TIMESTAMP,"\
                f" (select sof.id from states_of_closures sof where sof.description = 'imported' ),"\
                f" (select cr.id from closures cr where cr.hash = '{closure_hash}'))"
            print(query)
            cursor2.execute( query )
            self.connection_payments.commit()

            print(f"Cierre importado de instancia de minado id: {closures_id_mi}")
            print("Insertando estado del cierre de la instancia de minado en el historico")
            query = f"INSERT INTO historical_states_of_closures_mi (closure_mi,state,datetime)"\
                f" VALUES ({closures_id_mi},"\
                f" (select states_of_closures_mi.id from states_of_closures_mi where states_of_closures_mi.description = 'imported'),"\
                f" CURRENT_TIMESTAMP)"
            print(query)
            cursor2.execute( query )
            self.connection_payments.commit()

        except Exception as e:
           print(f"Fallo al importar el cierre {closure_hash} : Error-> {e}")
           return 0

        # recupero todos los farmers del cierre
        try:
            cursor.execute( f" select launcher_id , points from closures "\
                    f" join farmer_points_of_closure on ( closures.hash = farmer_points_of_closure.closure_hash ) where closures.hash = '{closure_hash}'" )
        except Exception as e:
            print(e)
        
        farmers_closure_active = cursor.fetchall()
        print(farmers_closure_active)
        
        # importo cada farmer de la lista y le agrego sus puntos
        
        for farmer_temp in farmers_closure_active:
            farmer_temp_launcher_id = farmer_temp[0]
            farmer_temp_points = farmer_temp[1]
            print(f" launcher_id {farmer_temp_launcher_id} puntos {farmer_temp_points}")
            farmer_temp_id = self.farmer_import(farmer_temp_launcher_id)
            print(farmer_temp_id)
            if farmer_temp_id != 0:
                print(f"Importando cierre")
                try:
                    cur = self.connection_payments.cursor()
                    # cur.execute(f"SELECT public.closing_points.points from public.closing_points  where closing_points.worker=(select workers.id from workers where worker_coin_id = '{farmer_temp_launcher_id}')")
                    # for i in cur.fetchone():
                    #     ins = i
                    # if ins == farmer_temp_points:
                    #     return 
                    query = f"INSERT INTO public.closing_points (points, worker, closure) "\
                        f"  VALUES( {farmer_temp_points} , (select workers.id from workers where worker_coin_id = '{farmer_temp_launcher_id}') , (select closures_of_mining_instance.id from closures_of_mining_instance inner join"\
                        f" closures on closures_of_mining_instance.closure = closures.id where closures.hash = '{closure_hash}')) ON CONFLICT (worker,closure)"\
                        f" DO UPDATE SET points = {farmer_temp_points} where closing_points.worker =(select workers.id from workers where worker_coin_id = '{farmer_temp_launcher_id}') and closing_points.closure = (select closures_of_mining_instance.id from closures_of_mining_instance inner join"\
                        f" closures on closures_of_mining_instance.closure = closures.id where closures.hash = '{closure_hash}')"
                    print(query)

                    cur.execute(query)
                    self.connection_payments.commit()
                    farmers_count = farmers_count + 1
                except Exception as e:
                    print(f"Error al insertar los puntos con su cierre : {e}")
        print("importando rewards by p2 singleton puzzle hash")

        # self.connection_payments.close()
        self.connection = self.connect_mariaDB_system() 
        cursor = self.connection.cursor()
        try:
            query = f"SELECT * FROM reward"
            print(query)
            cursor.execute( query )
            all_rewards = cursor.fetchall()
            print(all_rewards)
        except Exception as e:
            print(e)
        print(f"insertando rewards by p2 singleton puzzle hash en el sistema de pagos {len(all_rewards)}")
        if len(all_rewards):

            for r in all_rewards:
                # print(r[1])
                cursor.execute (f"SELECT * from reward where p2_singleton_puzzle_hash = '{r[1]}' ")
                print(cursor.fetchall())

                try:## revisar error {launcher_from_hash[0]} == None
                    cursor.execute(f"SELECT launcher_id from farmer where p2_singleton_puzzle_hash = '{r[1]}'")#--> CONSULTA ERRONEA?
                    launcher_from_hash = cursor.fetchone()
                    print(f"launcher_id = '{launcher_from_hash[0]}'")
                    query = f"INSERT INTO public.reward (timestamp, worker, amount, number, hash, closure) VALUES(CURRENT_TIMESTAMP,"\
                        f" (select workers.id from workers where workers.worker_coin_id = '{launcher_from_hash[0]}'),"\
                        f" ({r[5] / (10 ** 12)}),"\
                        f" ({r[2]}),"\
                        f" ('{r[1]}'),"\
                        f" (select closures.id from closures where hash = '{closure_hash}'))"
                    print(query)
                    cur = self.connection_payments.cursor()
                    cur.execute(query)
                    self.connection_payments.commit()
                    print(f"Inserciones totales deseadas : {len(all_rewards)} , insercion actual : {cur.lastrowid}")
                except psycopg2.Error as e:
                    print(e)
        # self.connection.close()
        # self.connection_payments.close()
            print("Seteamos la tabla reward a 0 despues de confirmar que hemos añadido los registros esperados al sistema de pagos CAMBIAR")# CAMBIAR EN PRODUCCIO
            # query = f"SELECT MAX(id) from public.reward"
            # cur.execute(query)
            # print(cur.fetchall())
            # query = f"DROP TABLE reward "
            # cursor.execute(query)
            # query = f"CREATE TABLE `reward` ("\
            #         "`id` int(11) NOT NULL AUTO_INCREMENT,"\
            #         "`p2_singleton_puzzle_hash` text DEFAULT NULL,"\
            #         "`confirmed_block_index` bigint(20) DEFAULT NULL,"\
            #         "`spent_block_index` bigint(20) DEFAULT NULL,"\
            #         "`puzzle_hash` text DEFAULT NULL,"\
            #         "`amount` double DEFAULT NULL,"\
            #         "`total_amount` double DEFAULT NULL,"\
            #         "`timestamp` timestamp NULL DEFAULT current_timestamp(),"\
            #         "PRIMARY KEY (`id`)"\
            #         ") ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;"
            # cursor.execute(query)
        return farmers_count
    
    def pay_to_farmer(self,launcher_id,generic_name,mount):
        mount_payout = 0
        worker_id = 0
        wallet_id = 0
        payments_id = 0
        additions_sub_list: List[Dict] = []
        pending = []
        try:
            self.connection_payments = self.connect_payments_system()
            cursor3 = self.connection_payments.cursor()
            # with self.connection_payments.transaction():
            # recupero el id del farmer
            query = f"select id from workers where worker_coin_id ='{launcher_id}'"
            cursor3.execute(query)
            print(f"Recupero el id del farmer: {query}")
            for i in cursor3.fetchone():
                worker_id = i
            print(f"farmer id recuperado : {worker_id}")
            if worker_id == 0:
                return mount_payout
        # con el ide obtengo la wallet del farmer
            query =  f"select w.id from workers"\
                        f" join wallets_of_workers wow on ( wow.worker = workers.id)"\
                        f" join states_of_wallets_of_workers sow on ( sow.id = wow.state)"\
                        f" join wallets w on ( wow.wallet = w.id)"\
                        f" join coins c on ( w.coin = c.id )"\
                        f" where sow.description = 'activa' and c.generic_mane = '{generic_name}' and workers.id = {worker_id} "
            print(f"Recupero el id del worker relacionado a la wallet: {query}")
            cursor3.execute(query)

            for w in cursor3.fetchone():
                wallet_id = w
            print(f"worker-wallet id recuperada {wallet_id}")#  f" join states_of_wallet sow on (sow.id = csow.states_of_wallet_id)"\
            if wallet_id == None:
                return mount_payout
        # creo el pago y recupero el id
            print(f"Insertando pago")
            query =  f"INSERT INTO public.payments (description, datetime_create, type_of_payment, wallet, mount, state, datetime_state) "\
                f" VALUES('farmer_chia_pay'::bpchar, CURRENT_TIMESTAMP, 1, '{wallet_id}' , {mount}, (select states_of_payments.id from states_of_payments where description = 'await'), (CURRENT_TIMESTAMP) )"
            print(query)
            cursor3.execute(query)
            print(f"Recupero payment id wallet")
            cursor3.execute("SELECT MAX(id) FROM payments")
            for p in cursor3.fetchone():
                payments_id = p
            print(f"payment id recuperado: {payments_id}")
            if payments_id == None:
                return mount_payout
            print("insertando historical_states_of_payments")
            query = f"INSERT INTO public.historical_states_of_payments (description,datetime, state, payment)"\
                f" VALUES ('farmer_chia_pay'::bpchar, CURRENT_TIMESTAMP,"\
                f" (select sof.id from states_of_payments sof where sof.description = 'await'),"\
                f" {payments_id})"
            cursor3.execute(query)
            self.connection_payments.commit()
        # creo el pago al farmer
            print(f"Insertando pago al farmer")
            query =  f"INSERT INTO public.payments_at_workers (payment, worker) "\
                f" VALUES('{payments_id}', '{worker_id}' )"
            print(query)
            cursor3.execute( query )
            self.connection_payments.commit()

            query = f"select w.hash from wallets w where w.id  = '{wallet_id}'"
            print(f"Recupero wallet hash: {query}")
            cursor3.execute(query)
            for wh in cursor3.fetchone():
                wallet_hash = wh
            print(f"wallet hash recuperado: {wallet_hash}")
            if wallet_hash == None:
                return mount_payout
            print(f"agrego el pago a la lista {wallet_hash}, {mount}")
            additions_sub_list.append({"puzzle_hash": wallet_hash, "amount": mount})
            pending.append(additions_sub_list.copy())
            print(f"el pago fue agregado con exito {additions_sub_list}")
            # self.connect_payments_system.commit()
        except Exception as e:
            print(f"Error exception in pay to farmer : {e}")
            return 0
        return mount

    def pay_closures_with_fe(self,closure_hash,fe):
        acumulated_amount = 0
        pool_amount = 0
        farmers_amount = 0
        total_points = 0
        pay_to_point = 0
        pay_amount = 0
        pay_to_farmers = 0
        try:
            self.connection_payments = self.connect_payments_system()
            cursor3 = self.connection_payments.cursor()
            print("seleccionamos el monto total de la tabla rewards ")
        # recupero el monto acumulado del cierre
            query = f"SELECT SUM(amount) from reward inner join closures on reward.closure = closures.id inner join closures_of_mining_instance comi on closures.id = comi.closure where closures.hash = '{closure_hash}'"
            cursor3.execute(query)
            ac = cursor3.fetchone()
            acumulated_amount = ac[0]

            print(f"El monto recuperado: {acumulated_amount}")
    # recupero los farmers del cierre junto con sus puntos y wallets
            query = f" select w.hash as wallet_hash, workers.worker_coin_id as launcher_id , cp.points "\
                    f" from workers "\
                    f" join wallets_of_workers wow on ( wow.worker = workers.id) "\
                    f" join states_of_wallets_of_workers soww on (soww.id = wow.state) "\
                    f" join wallets w on ( wow.wallet = w.id) "\
                    f" join coins c on ( w.coin = c.id ) "\
                    f" join closing_points cp on (cp.worker = workers.id)  "\
                    f" join closures_of_mining_instance comi on (comi.id = cp.closure) "\
                    f" join mining_instances mi on (comi.mining_instance = mi.id) "\
                    f" join closures cr on ( cr.id = comi.closure) "\
                    f" where soww.description = 'activa' and c.generic_mane = 'XCH' and comi.closure = (select cr.id from closures cr where cr.hash = '{closure_hash}' )" 
            print(f"Recupero los datos de pago de los farmers: {query}")
            cursor3.execute(query)
            payment_details = cursor3.fetchall()
            print(payment_details)

    # saco el porcentaje para la pool
            pool_amount = acumulated_amount * fe
            farmers_amount = acumulated_amount - pool_amount
    # calculo el pago por punto y realizo los pagos
            for payment_detail in payment_details:
                total_points = total_points + payment_detail[2]
            pay_to_point = farmers_amount / total_points

            for payment_detail in payment_details:
                pay_to_farmers = pay_to_point * payment_detail[2]
                self.pay_to_farmer(payment_detail[1],'XCH',pay_to_farmers)
                pay_amount = pay_amount + pay_to_farmers
                pay_to_farmers = 0
        except Exception as e:
            print(e)
            return 0
        
        return pay_amount,acumulated_amount

    def delete_n_closures(self, number_of_closures_keep):
        self.connection_payments = self.connect_payments_system()
        cursor_payments = self.connection_payments.cursor()
        self.connection = self.connect_mariaDB_system() 
        cursor_pool = self.connection.cursor()
        query = f"DELETE FROM closures where id <= ((select MAX(id) from closures) - {number_of_closures_keep})"
        cursor_payments.execute(query)
        query = f"DELETE FROM closures WHERE hash NOT IN (SELECT hash FROM(SELECT hash FROM closures ORDER BY timestamp DESC LIMIT {int(number_of_closures_keep)})foo);"
        cursor_pool.execute(query)
        self.connection.commit()
        self.connection_payments.commit()
        cursor_pool.close()
        cursor_payments.close()
        self.connection_payments.close()
        self.connection.close()
        return f"Keep {int(number_of_closures_keep)} closures"
    
    def show_closures(self,n_closures_to_show):
        ids = []
        rows = []
        self.connection_payments = self.connect_payments_system()
        cursor_payments = self.connection_payments.cursor()
        query =  f"SELECT cr.id from closures cr where cr.id >  ((select MAX(cr.id) from closures cr) - {int(n_closures_to_show)})"
        cursor_payments.execute(query)
        for id in cursor_payments.fetchall():
            ids.append(id[0])
        for id in ids: 
            query = f"SELECT (select cr.hash from closures cr where cr.id = {id}),COUNT(cp.worker),SUM(r.amount) from closing_points cp inner join closures_of_mining_instance comi on cp.closure = comi.id inner join closures cr on comi.closure = cr.id inner join reward r on r.closure = cr.id where cr.id = {id}"
            cursor_payments.execute(query)
            rows.append(cursor_payments.fetchall())
        cursor_payments.close()
        self.connection_payments.close()
        return rows
    def get_farmer_points_by_closure_id(self, closure_id, start, end):
        response = []
        self.connection_payments = self.connect_payments_system()
        cursor_payments = self.connection_payments.cursor()
        query = f"SELECT w.worker_coin_id, cp.points from workers w inner join closing_points cp on w.id = cp.worker"\
            f" inner join closures_of_mining_instance comi on cp.closure = comi.id"\
            f" inner join closures c on comi.closure = c.id where c.id = {closure_id} and w.id >= {start} and w.id <= {end} order by cp.points desc"
        cursor_payments.execute(query)
        rows = cursor_payments.fetchall()
        for row in rows: 
            response.append(row)
        cursor_payments.close()
        self.connection_payments.close()
        return response
    
    def delete_parials_time(self,time_utc):
        self.connection = self.connect_mariaDB_system()
        cursor_pool = self.connection.cursor()
        timestamp = datetime.strptime(time_utc,"%Y-%m-%dT%H:%M:%SZ").timestamp()
        query = f"DELETE from partial where `timestamp`< {timestamp}"
        cursor_pool.execute(query)
        self.connection.commit()
        cursor_pool.execute(f"select count(*) from partial")
        resp = cursor_pool.fetchone()
        cursor_pool.close()
        self.connection.close()
        return resp

    def partial_import(self, time_utc):
        self.connection = self.connect_mariaDB_system()
        cursor_pool = self.connection.cursor()
        timestamp = datetime.strptime(time_utc,"%Y-%m-%dT%H:%M:%SZ").timestamp()
        query = f"INSERT into historical_partials(launcher_id,timestamp,difficulty) SELECT launcher_id,timestamp,difficulty from partial where `timestamp` < {timestamp} "
        cursor_pool.execute(query)
        self.connection.commit()
        cursor_pool.execute(f"select count(*) from historical_partials")
        resp = cursor_pool.fetchone()
        cursor_pool.close()
        self.connection.close()
        return resp
        
