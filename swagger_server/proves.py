import asyncio
from asyncio import Task
from math import floor
from sqlite3.dbapi2 import complete_statement
from typing import Dict, Optional, Set, List, Tuple, Callable
import os
from pymysql import cursors
import yaml
from datetime import datetime,date
import hashlib
from pathlib import Path
import sqlite3
import asyncpg
from datetime import date
import time
import math
from datetime import datetime
from blspy import AugSchemeMPL, G1Element
import psycopg2
import pymysql
from chia.types.blockchain_format.sized_bytes import bytes32
import requests
from collections import defaultdict

class ManagementSystem:
    def __init__(self):
        with open(r"C:\Users\Aleix\Desktop\api_mgmt\api_mgmt\swagger_server\config.yaml") as f:
            pool_config: Dict = yaml.safe_load(f)
        self.pool_config = pool_config

        self.pool_config = pool_config
        config_path_db = r"C:\Users\Aleix\Desktop\api_mgmt\api_mgmt\swagger_server\pooldb.sqlite"
        self.connection: Optional[sqlite3.Connection] = None
        print(f"Configurando base de datos: {config_path_db}")

        if (config_path_db) :
            self.db_path = Path(config_path_db)
        else:
            self.db_path = Path("pooldb.sqlite")
        self.payment_interval = pool_config["payment_interval"]
        # print("Preparando base de datos MAria DB y payments")
        # self.connection_mariaDB: Optional[pymysql.Connection] = None
        # self.connection_payments: Optional[asyncpg.Connection] = None
        print("Preparando base de datos sqlite y paydb de prueba")
        self.connection: Optional[sqlite3.Connection] = None # cambiar a maria DB !!!!!!!!
        self.connection_payments: Optional[psycopg2.Connection] = None
        self.pool_fee = pool_config["pool_fee"]   
        self.scan_p2_singleton_puzzle_hashes: Set[bytes32] = set()
        self.pending_payments: Optional[asyncio.Queue] = None
        self.number_of_partials_target: int = pool_config["number_of_partials_target"]

    def connect_payments_system(self):
        host=self.pool_config["payouts"]["database"]["host"]
        port=self.pool_config["payouts"]["database"]["port"]
        user=self.pool_config["payouts"]["database"]["user"]
        password=self.pool_config["payouts"]["database"]["password"]
        db=self.pool_config["payouts"]["database"]["db"]

        print(f" PUERTO {port}  ")

        print(f" {host}  {port}   {user}   {password}   {db}  ")
        self.connection_payments = psycopg2.connect(host=host,port=port,user=user,password=password,database=db)
        return self.connection_payments

    def connect_mariaDB_system(self):
        hostM=self.pool_config["mariadb"]["database"]["host"]
        portM=self.pool_config["mariadb"]["database"]["port"]
        userM=self.pool_config["mariadb"]["database"]["user"]
        passwordM=self.pool_config["mariadb"]["database"]["password"]
        dbM=self.pool_config["mariadb"]["database"]["db"]

        print(f" PUERTO {portM}  ")

        print(f" {hostM}  {portM}   {userM}   {passwordM}   {dbM}  ")
        self.connection = pymysql.connect(host=hostM,port=portM,user=userM,password=passwordM,database=dbM)
        return self.connection
    def write(self,data):
        hash_to_show = []
        amounts = []
        farmers = []
        resp = defaultdict(list)
        self.connection_payments = self.connect_payments_system()
        cursor_payments = self.connection_payments.cursor()
        query =  f"SELECT hash from closures where id <=  ((select MAX(id) from closures) - {data})"
        cursor_payments.execute(query)
        rows = cursor_payments.fetchall()
        for row in rows:
            hash_to_show.append(row[0])
            # resp["closure_hash"].append(row[0])
        for i in hash_to_show:
            query = f"SELECT SUM(amount) from reward r inner join closures cr on r.closure = cr.id where cr.hash = '{i}'"
            cursor_payments.execute(query)
            am = cursor_payments.fetchall()
            for a in am:
                amounts.append(a[0])
                # resp["amount"].append(a[0])
            query =  f"SELECT COUNT(worker) from closing_points cp inner join closures_of_mining_instance comi on cp.closure = comi.id"\
                f" inner join closures cr on comi.closure = cr.id where cr.hash = '{i}' "
            cursor_payments.execute(query)
            count = cursor_payments.fetchall()
            for c in count:
                farmers.append(c[0])
                # resp["farmers"].append(c[0])
            # farmers.append(c[0] for c in count)
        print(hash_to_show)
        print(amounts)
        print(farmers)
        for r in range(0, len(hash_to_show)):
            resp["closures_hash"].append(hash_to_show[r])
            resp["amount"].append(amounts[r])
            resp["farmers"].append(farmers[r])
        print(resp.items())
     
        return 'ok'
    def prova(self, closure_id, start, end):
        self.connection = self.connect_mariaDB_system()
        cursor_pool = self.connection.cursor()
        time_utc = "2021-10-18T16:57:19Z"
        timestamp = datetime.strptime(time_utc,"%Y-%m-%dT%H:%M:%SZ").timestamp()
        query = f"DELETE from partial where `timestamp`< {timestamp}"
        cursor_pool.execute(query)
        self.connection.commit()

        print(timestamp)
mgmt = ManagementSystem()
hash = mgmt.prova(187,0,100)
# print(hash)
# # #     print (h)
# d = date(year=2021,month=10,day=5)
# print(date.today())
